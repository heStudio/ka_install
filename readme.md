# kali install for Termux 

## 本次更新
使用全新3.0.0版本脚本

## 作者

### [醉、倾城](https://www.hestudio.net/about)

- 联系邮箱：hestudio@hestudio.net
- QQ频道：https://pd.qq.com/s/uakgta
- 作者CSDN：醉、倾城
- heStudio社区：https://bbs.csdn.net/forums/hestudio
- 作者Gitee：https://gitee.com/heStudio
- 作者Github：https://github.com/heStudio-Network
- 作者博客：https://www.hestudio.net/
- 作者爱发电：https://afdian.net/@hestudio
- RSS 订阅：https://www.hestudio.net/atom.xml
- 问题反馈：https://www.hestudio.net/get-help
- heStudio Talking: https://www.hestudio.net/talking

## 更新代号
* 更新代号：3.0.0

## 传送门
### 教程
https://www.hestudio.net/posts/install-kali-on-android-renew.html

### 文档
https://www.hestudio.net/docs/kali_for_android.html

### 怎么获取key&找到已购买的key
https://www.hestudio.net/docs/key.html
 
### 官方文档
https://www.kali.org/docs/nethunter/nethunter-rootless/


# 赞助(赞助资金全部用来续费服务器)
https://www.hestudio.net/donate
