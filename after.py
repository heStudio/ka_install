def main():
    print("""
kali install for Termux 
    
作者：醉、倾城@heStudio
    
联系邮箱：hestudio@hestudio.net
QQ频道：https://pd.qq.com/s/uakgta
作者CSDN：醉、倾城
heStudio社区：https://bbs.csdn.net/forums/hestudio
作者Gitee：https://gitee.com/heStudio
作者Github：https://github.com/heStudio-Network
作者博客：https://www.hestudio.net/
作者爱发电：https://afdian.net/@hestudio
RSS 订阅：https://www.hestudio.net/atom.xml
问题反馈：https://www.hestudio.net/get-help
heStudio Talking: https://www.hestudio.net/talking

感谢对我的支持，老版本的vnc快捷代码将不再提供。如果你想继续使用，请访问 https://www.hestudio.net/posts/ka_install_patch_230603.html

本版本已经预装原生的TigerVNCServer，你可以使用最新版本的VNCServer自带的原生代码，详情请浏览 https://www.hestudio.net/docs/kali_for_android.html#%E4%B8%80%E4%BA%9B%E5%91%BD%E4%BB%A4

heStudio
2023.6.3

    """)
