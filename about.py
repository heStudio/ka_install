import os
import sys
import time
import json

# 版本号使用语义化标准格式
SIGN="3.0.0"
# UPDATE_DATE="2023.6.8"

def output_info():
    os.system("clear")
    print("""
kali install for Termux 
    
作者：醉、倾城@heStudio
    
联系方式详见：https://www.hestudio.net/about

问题反馈：https://www.hestudio.net/get-help

heStudio Talking: https://www.hestudio.net/talking
    
    """)
    
    for ad in ads["ads"]:
        print("[广告]",ad)
    print("""
安装之前，请你重新阅读：https://www.hestudio.net/posts/install-kali-on-android-renew.html 
具体的改动以上面的链接为准！

如果遇到问题，请先浏览 https://www.hestudio.net/docs/kali_for_android.html
如果遇到的问题文档里没有，请到 https://www.hestudio.net/get-help 反馈。
如果反馈的问题在5个小时没有回复（阴间时间除外），请进QQ频道。
QQ频道： https://pd.qq.com/s/uakgta
  
   
赞助的所有资金用于更好的提供服务。毕竟都不想用着卡顿的服务器下载文件。

怎么获取key&找到已购买的key: https://www.hestudio.net/docs/key.html

    """)

    print("版本号:", SIGN)
    print("输入key后将开始安装进程。")
    
def keyverify(key, phone):
    print("正在请求服务器验证key...")
    url = str(r"https://api.hestudio.net/keyverify\?"+"key\="+key+"\&phone\="+phone)
    api_return = os.popen(str("curl "+url))
    time.sleep(5)
    data = json.load(api_return)
    if data["verify"] == True: 
        print("验证成功，正在请求下载链接...")
        down_url = str(r"https://api.hestudio.net/kali_download\?"+"key\="+key+"\&token\="+data["token"])
        print("已获取下载链接，正在运行在线资源...")
        scriptfile=open('.cache','w')
        scriptfile.write(str('bash -c "$(curl '+down_url+')"'))
        time.sleep(1)
        os.system("chmod +x ./finaltouchup.sh")
        scriptfile.close()
        time.sleep(1)
        os.system("chmod +x ./.cache")
        os.system("./.cache")
        print("安装程序结束，正在作最后的整理...")
        os.system("rm .cache")
        os.system("startkali")
    elif data["verify"] == "lock":
        os.system('clear')
        print("""
你的key输入没有错误，但是你的key已被锁定，这可能是因为你异常使用造成的，请联系我们申诉解决：hestudio@hestudio.net

请尽量提供你的交易记录，短信截图等有助于你申诉成功的材料。

你可随时在 https://www.hestudio.net/docs/key.html 的 查找你的Key 板块查询key状态。
""")
        sys.exit()
    else:
        print("验证失败，请检查你的key和信息有没有输入错误，请检查输入的信息内是否包含空格，然后通过运行 ./install.sh 重新安装。")
        sys.exit()
    

if __name__ == "__main__":
    print("Loading...")
    version_get=os.popen(str("curl https://api.hestudio.net/kaliscript_version"))
    
    # 广告接口接入heStudio体系
    ads_get=os.popen(str("curl https://api.hestudio.net/ad/v2"))
    ads = json.load(ads_get)
    version = json.load(version_get)
    
    # 重写版本验证模块
    print("正在请求验证版本....")
    time.sleep(5)
    if not SIGN in version["version"]:
        print("\r此版本过于老旧，请删除ka_install文件夹和install.sh脚本，然后再按照教程重新开始安装！")
        print("\n附教程链接：https://www.hestudio.net/posts/install-kali-on-android-renew.html")
        sys.exit()
    else:
        pass
    
    output_info()
    key_input = input("\r输入获取的key：")
    phone_input = input("\r输入购买key预留的手机号：")
    print("\n\nLoading...")
    keyverify(key_input, phone_input)
